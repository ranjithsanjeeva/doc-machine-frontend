import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-task',
  templateUrl: './dashboard-task.component.html',
  styleUrls: ['../../../sass/application.scss', './dashboard-task.component.scss']
})
export class DashboardTaskComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
